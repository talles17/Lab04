RM = rm -rf

CC = g++


LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test


CFLAGS = -Wall -ansi -pedantic -std=c++11 -I. -I$(INC_DIR)

.PHONY: all


all: nascimentos

debug: CFLAGS += -g -O0
debug: nascimentos



nascimentos: $(OBJ_DIR)/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'nascimentos' criado em $(BIN_DIR)] +++"
	@echo "============="	


$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


clean: 
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*